# Live Munitions
It was inevitable.

## Users
This mod adds a new Shell in the ammo menu of any mortar that allows Live Munitions (babies) to be loaded up and fired like any other shell.  
Doing this will have a number of psychological impacts on the map.  
The parents of a projectile will despise the gunner who hurled their kid accross the map. Other colonists will also be less than thrilled, but will get over it soon enough.  
The gunner themselves will be unhappy about their actions.  

If a normal projectile is used, the resulting impact will be highly disturbing to any nearby people, in addition to potentially harming them (and expending the shell.)  
A number of new genes corresponding to existing mortar shell types are also available. Any colonist who is subjected to these genes will be very unhappy with their newfound existence as a mortar shell. They will also violently explode when killed, according to the gene they have been inflicted with.  


## Modders

### Adding new genes
There are essentially 3 parts to turning a pawn into a projectile:
1. A relevant gene that can be used to mark them as an explosive - this could even be vanilla genes
2. A new ThingDef using <thingClass>BabyMortar.Projectile_Pawn</thingClass>, and any custom projectile you like - this will also define the explosion upon death.
3. A BabyMortar.PawnProjectileDef entry containing the new gene in <gene></gene> tags, and projectileWhenLoaded pointing to the above projectile
Once these are added, a sufficiently small pawn containing the new gene may be stuffed into a mortar and fired like any other projectile.  

### Psychological damage
There are several thoughtDefs associated with PawnProjectileDef that can be used to tweak how people react to the use of human projectiles.
 - <parentThought> - A thought given to parents when their child is used as munitions
 - <colonistThought> - A thought given to all other colonists when any child is used as munitions
 - <parentThoughtForGunner> - A social thought given to the parents against the gunner who used their child as munitions
 - <colonistThoughtForGunner> - A social thought given to all other colonists against the gunner who used any child as munitions
 - <gunnerThought> - A thought just for the gunner themselves
 - <psychologicalImpactRadius> - If thoughtInAreaOfImpact is provided, this is the range in which all pawns will receive that thought
 - <thoughtInAreaOfImpact> - A thought given to any pawn within the area of impact
