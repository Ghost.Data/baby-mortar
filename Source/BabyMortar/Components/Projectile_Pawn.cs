﻿using HarmonyLib;
using RimWorld;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using Verse.AI;

namespace BabyMortar {
    public class Projectile_Pawn : Projectile_Explosive {
        public Pawn pawn;
        private Traverse tweenerTraverse;
        public PawnProjectileDef shellDef;
        public IntVec3 lastPosition;
        public Map lastMap;

        public Pawn Gunner {
            get => launcher as Pawn;
        }

        public override void ExposeData() {
            base.ExposeData();
            Scribe_Defs.Look(ref shellDef, "shellDef");
            Scribe_References.Look(ref pawn, "pawn");
           
        }

        public override void Launch(Thing launcher, Vector3 origin, LocalTargetInfo usedTarget, LocalTargetInfo intendedTarget, ProjectileHitFlags hitFlags, bool preventFriendlyFire = false, Thing equipment = null, ThingDef targetCoverDef = null) {
            // By the time this class is spawned, the parent shell def is already gone, so we store it in LiveMunitionsComp.
            pawn = (equipment as Building_TurretGun).gun.TryGetComp<LiveMunitionsComp>().StoredPawn;
            pawn.holdingOwner.Remove(pawn);
            
            shellDef = (equipment as Building_TurretGun).gun.TryGetComp<LiveMunitionsComp>().lastLoadedShellDef;
            tweenerTraverse = Traverse.Create(pawn.Drawer.tweener);
            GenPlace.TryPlaceThing(pawn, origin.ToIntVec3(), launcher.Map, ThingPlaceMode.Direct);
            pawn.jobs.StopAll();
            pawn.jobs.StartJob(JobMaker.MakeJob(LMJobDefOf.BeAShellJob, pawn), JobCondition.InterruptForced);

            base.Launch(launcher, origin, usedTarget, intendedTarget, hitFlags, preventFriendlyFire, equipment, targetCoverDef);
        }

        public override void Tick() {
            if (tweenerTraverse == null) {
                tweenerTraverse = Traverse.Create(pawn.Drawer.tweener);
            }
            lastPosition = Position;
            lastMap = Map;
            base.Tick();
            // Updating the tweened position accurately places the pawn where the shell would normally be.
            Vector3 pos = ExactPosition;
            tweenerTraverse.Field("tweenedPos").SetValue(pos);
            pawn.Position = Position;
        }

        protected override void Impact(Thing hitThing, bool blockedByShield = false) {
            base.Impact(hitThing, blockedByShield);
            PsychologicalDamage();
            //While the explosion itself often kills the pawn projectile, do some landing damage just in case
            HealthUtility.DamageUntilDead(pawn);
        }

        /// <summary>
        /// Assign applicable memories to everyone involved in this horrible event
        /// </summary>
        private void PsychologicalDamage() {
            if (pawn == null || shellDef == null) {
                return;
            }
            Pawn father = pawn.GetFather();
            Pawn mother = pawn.GetMother();

            if (!Settings.yeetMode && father?.needs?.mood?.thoughts?.memories != null) {
                if (shellDef.parentThought != null) {
                    father.needs.mood.thoughts.memories.TryGainMemory(shellDef.parentThought);
                }
                if (shellDef.parentThoughtForGunner != null) {
                    father.needs.mood.thoughts.memories.TryGainMemory(shellDef.parentThoughtForGunner, Gunner);
                }

            }

            if (!Settings.yeetMode && mother?.needs?.mood?.thoughts?.memories != null) {
                if (shellDef.parentThought != null) {
                    mother.needs.mood.thoughts.memories.TryGainMemory(shellDef.parentThought);
                }

                if (shellDef.parentThoughtForGunner != null) {
                    mother.needs.mood.thoughts.memories.TryGainMemory(shellDef.parentThoughtForGunner, Gunner);
                }
            }

            if (shellDef.colonistThought != null || shellDef.colonistThoughtForGunner != null) {

                List<Pawn> colonists = lastMap.mapPawns.FreeColonistsAndPrisoners.Where(p => p != Gunner && p != father && p != mother).ToList();
               
                foreach (Pawn colonist in colonists) {
                   
                    if (!Settings.yeetMode && shellDef.colonistThought != null && colonist?.needs?.mood?.thoughts?.memories != null) {
                        colonist.needs.mood.thoughts.memories.TryGainMemory(shellDef.colonistThought);
                    }

                    if (!Settings.yeetMode && shellDef.colonistThoughtForGunner != null && colonist?.needs?.mood?.thoughts?.memories != null) {
                        colonist.needs.mood.thoughts.memories.TryGainMemory(shellDef.colonistThoughtForGunner, Gunner);
                    }
                }
            }

            if (!Settings.yeetMode && shellDef.gunnerThought != null && Gunner != null) {
                Gunner.needs.mood.thoughts.memories.TryGainMemory(shellDef.gunnerThought);
            }

            ProcessImpactPawnThoughts();
        }

        /// <summary>
        /// If enabled, assign the impact thought to any pawns within a certain radius of the impact site.
        /// </summary>
        private void ProcessImpactPawnThoughts() {
            if (shellDef.thoughtInAreaOfImpact != null) {
                if (lastPosition != null && lastMap != null) {
                    List<Thing> thingsNearby = GenRadial.RadialDistinctThingsAround(lastPosition, lastMap, shellDef.psychologicalImpactRadius, true).ToList();

                    foreach (Thing thing in thingsNearby) {
                        if (thing is Pawn impactedPawn && impactedPawn.needs?.mood?.thoughts?.memories != null) {
                            impactedPawn.needs.mood.thoughts.memories.TryGainMemory(shellDef.thoughtInAreaOfImpact);
                            impactedPawn.needs.mood.CurLevel -= 20f;
                        }
                    }
                }
            }
        }
    }
}
