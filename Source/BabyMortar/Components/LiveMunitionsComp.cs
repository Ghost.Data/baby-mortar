﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace BabyMortar {
    /// <summary>
    /// Because mortars store ammo as a def rather than an instance, we will use this 
    /// component - attached to the mortar - to store the pawn ammo so it isn't lost.
    /// </summary>
    public class LiveMunitionsComp : ThingComp, IThingHolder {
        public ThingOwner<Pawn> container;
        public Pawn storedPawn;
        public PawnProjectileDef lastLoadedShellDef;

        public LiveMunitionsComp() {
            container = new ThingOwner<Pawn>();
        }

        public Pawn StoredPawn => (container != null && container.Count > 0) ? container.InnerListForReading?.First() : null;

        public override void PostSpawnSetup(bool respawningAfterLoad) {
            base.PostSpawnSetup(respawningAfterLoad);
            storedPawn = (container.Count > 0) ? container.InnerListForReading?.First() : null;;
        }



        public override void PostExposeData() {
            base.PostExposeData();
            Scribe_Deep.Look(ref this.container, "container", this);
            
        }

        public void GetChildHolders(List<IThingHolder> outChildren) {
            ThingOwnerUtility.AppendThingHoldersFromThings(outChildren, GetDirectlyHeldThings()); ;
        }

        public ThingOwner GetDirectlyHeldThings() {
            return container;
        }
    }

    public class CompProperties_LiveMuntions : CompProperties {
        public CompProperties_LiveMuntions() {
            this.compClass = typeof(LiveMunitionsComp);
        }

        public CompProperties_LiveMuntions(Type compClass) : base(compClass) {
            this.compClass = compClass;
        }
    }
}
