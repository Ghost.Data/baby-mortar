﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.Sound;

namespace BabyMortar {
    public class TurretPatches {
        /// <summary>
        /// Allow the mortar operator to drag pawns to the tube.
        /// Since we only override if no ammo was found, it will create a psuedo priority 
        /// for real shells instead of pawns.
        /// </summary>
        [HarmonyPatch(typeof(JobDriver_ManTurret), "FindAmmoForTurret")]
        public static class JobDriver_ManTurret_FindAmmoForTurret_Patch {
            public static void Postfix(Pawn pawn, Building_TurretGun gun, ref Thing __result) {
                if (__result == null) {
                    StorageSettings allowedShellsSettings = pawn.IsColonist ? gun.gun.TryGetComp<CompChangeableProjectile>().allowedShellsSettings : null;
                    if (allowedShellsSettings.AllowedToAccept(PawnProjectileDefOf.Shell_Infant)) {
                        Predicate<Thing> validator = t => !t.IsForbidden(pawn) && pawn.CanReserve(t, 10, 1) && Util.IsValidMunitions(t as Pawn);
                        __result = GenClosest.ClosestThingReachable(gun.Position, gun.Map, ThingRequest.ForGroup(ThingRequestGroup.Pawn), PathEndMode.OnCell, TraverseParms.For(pawn), 40f, validator);
                    }
                }
            }
        }

        /// <summary>
        /// Override loadShell toil so we can prevent the pawn from being deleted and inject our
        /// own handler.
        /// </summary>
        public static class JobDriver_ManTurret_MakeNewToils_Patch {
            public static bool Prefix(object __instance) {
                Toil loadShell = (Toil)Traverse.Create(__instance).Field("loadShell").GetValue();
                Pawn jobPawn = loadShell.actor;
                Building_TurretGun mortar = jobPawn.CurJob.targetA.Thing as Building_TurretGun;
                Thing carriedMunitions = jobPawn.CurJob.targetB.Thing;
                LiveMunitionsComp comp = mortar.gun.TryGetComp<LiveMunitionsComp>();
                if (carriedMunitions is Pawn && comp != null) {
                    SoundDefOf.Artillery_ShellLoaded.PlayOneShot((SoundInfo)new TargetInfo(mortar.Position, mortar.Map));
                    PawnProjectileDef bestShell = Util.GetMostAppropriateProjectileType(carriedMunitions as Pawn);
                    mortar.gun.TryGetComp<CompChangeableProjectile>().LoadShell(bestShell, 1);
                    comp.storedPawn = carriedMunitions as Pawn;
                    comp.lastLoadedShellDef = bestShell;
                    jobPawn.carryTracker.innerContainer.TryTransferToContainer(carriedMunitions, comp.container);
                    ((Pawn)carriedMunitions).jobs.StopAll(true, false);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Return our stored pawn instead of a new shell instance
        /// </summary>
        [HarmonyPatch(typeof(CompChangeableProjectile), "RemoveShell")]
        public static class CompChangeableProjectile_RemoveShell_Patch {
            public static bool Prefix(ref CompChangeableProjectile __instance, ref Thing __result) {
                LiveMunitionsComp comp = __instance.parent.GetComp<LiveMunitionsComp>();
                if (comp?.StoredPawn != null) {
                    __instance.loadedCount = 0;
                    Traverse.Create(__instance).Field("loadedShell").SetValue(null);
                    __result = comp.container.Take(comp.StoredPawn);
                    comp.storedPawn = null;

                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Prevent pawns from taking out our ammo due to the spoof
        /// </summary>
        [HarmonyPatch(typeof(StorageSettings), "AllowedToAccept", new[] {typeof(ThingDef)})]
        public static class StorageSettings_AllowedToAccept_Patch {
            public static bool Prefix(ThingDef t, ref bool __result, StorageSettings __instance) {
                if (DefDatabase<PawnProjectileDef>.AllDefsListForReading.Contains(t) && t != PawnProjectileDefOf.Shell_Infant) {
                    __result = __instance.AllowedToAccept(PawnProjectileDefOf.Shell_Infant);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Because we want to have pawns with explosive genes pop when they die from non-projectile
        /// causes as well, here we will make sure a pawn isn't already a projectile and then trigger
        /// an explosion manually. Projectile pawns will already explode.
        /// </summary>
        [HarmonyPatch(typeof(Pawn), "Kill")]
        public static class Pawn_Kill_Patch {
            public static void Prefix(Pawn __instance, DamageInfo? dinfo, Hediff exactCulprit = null) {
                PawnProjectileDef bestShell = Util.GetMostAppropriateProjectileType(__instance);
                if ((bestShell == PawnProjectileDefOf.Shell_Infant && Util.IsAProjectile(__instance))
                    || (bestShell != null && bestShell != PawnProjectileDefOf.Shell_Infant)) {
                    Util.Explode(__instance, bestShell.projectileWhenLoaded.projectile);

                }
            }
        }
    }
}
