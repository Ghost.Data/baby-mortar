﻿using RimWorld;
using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace BabyMortar {
    public class BeAShellJob : JobDriver {
        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return true;
        }

        protected override IEnumerable<Toil> MakeNewToils() {
            Toil wait = new Toil();
            wait.initAction = delegate {
                PawnUtility.ForceWait(TargetA.Thing as Pawn , 1000);
            };
            yield return wait;
        }
    }
}
