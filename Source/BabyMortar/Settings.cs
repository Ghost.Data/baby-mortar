﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace BabyMortar {
    internal class Settings : ModSettings {
        public static bool yeetMode = false;

        public override void ExposeData() {
            base.ExposeData();
            Scribe_Values.Look(ref yeetMode, "yeetMode");
        }
    }
}
