﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using static BabyMortar.TurretPatches;

namespace BabyMortar {
    /// <summary>
    /// What it says on the tin. It was inevitable.
    /// </summary>
    public class BabyMortar : Mod {
        public static BabyMortar instance;
        private string[] buffers = new string[10];
        public BabyMortar(ModContentPack content) : base(content) {
            var harmony = new Harmony("BabyMortar");
            harmony.PatchAll();
            instance = this;

            //Track down the anonymous method used for the loadShell toil
            MethodInfo initActionAnonMethod = typeof(JobDriver_ManTurret).GetNestedTypes(AccessTools.all).Where(t => t.Name.Contains("<>c__DisplayClass")).SelectMany(AccessTools.GetDeclaredMethods).FirstOrDefault(m => m.Name == "<MakeNewToils>b__2");
            harmony.Patch(initActionAnonMethod, new HarmonyMethod(AccessTools.Method(typeof(JobDriver_ManTurret_MakeNewToils_Patch), "Prefix")));



        }

        public override string SettingsCategory() {
            return "Live Munitions";
        }

        public override void DoSettingsWindowContents(Rect inRect) {
            Widgets.DrawMenuSection(inRect);
            Listing_Standard list = new Listing_Standard();
            list.Begin(inRect);

            list.CheckboxLabeled("SettingsYeet".Translate(), ref Settings.yeetMode, "SettingsYeet_Desc".Translate());

            list.End();
        }
    }
}
