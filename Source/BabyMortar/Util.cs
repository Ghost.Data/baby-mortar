﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace BabyMortar {
    public class Util {
        /// <summary>
        /// Determines if a pawn counts as a valid shell.
        /// </summary>
        /// <param name="pawn">The pawn to check</param>
        /// <returns>True if the pawn is valid to be stuffed into a mortar</returns>
        public static bool IsValidMunitions(Pawn pawn) {
            return pawn.BodySize <= 0.3f && (pawn.IsColonist || pawn.IsPrisonerOfColony || pawn.IsSlaveOfColony) && !IsAProjectile(pawn);
        }

        /// <summary>
        /// Because we are spoofing the shell loading system to use one shell thingdef for loading, and 
        /// many for firing, this method will find the appropriate shell type to use for the pawn based
        /// on their genes.
        /// </summary>
        /// <param name="pawn">A valid gene filled pawn</param>
        /// <returns>The most appropriate shell type def, a random one if multiple are valid.</returns>
        public static PawnProjectileDef GetMostAppropriateProjectileType(Pawn pawn) {
            PawnProjectileDef shelldef = null;
            IEnumerable<GeneDef> pawnGenes = pawn?.genes?.GenesListForReading?.Select(g => g.def);
            if (pawnGenes != null) {
                shelldef = DefDatabase<PawnProjectileDef>.AllDefsListForReading.Where(def => pawnGenes.Contains(def.gene)).RandomElement();
                if (shelldef == null) {
                    shelldef = DefDatabase<PawnProjectileDef>.AllDefsListForReading.Find(def => def.gene == null);
                }
            }
          

            return shelldef;
        }

        /// <summary>
        /// Because we have an interest in determining if a pawn was killed via a launch, and no other
        /// handier way to determine that, we'll instead try to find the projectile dummy and use that
        /// to determine if the pawn was recently a projectile.
        /// </summary>
        /// <param name="pawn">The potentially flying pawn</param>
        /// <returns>true only if the pawn is in the same cell as their projectile object</returns>
        public static bool IsAProjectile(Pawn pawn) {
            return pawn?.Map?.listerThings?.AllThings?.Where(t => t is Projectile_Pawn proj && proj.pawn == pawn)?.Any() ?? false;

        }

        /// <summary>
        /// Trigger the same explosion effect that a pawn would have if launched, sans the launcher.
        /// </summary>
        /// <param name="pawn">The pawn to explode.</param>
        /// <param name="projectile">The def containing the project properties the determine the explosion.</param>
        public static void Explode(Pawn pawn, ProjectileProperties projectile) {
            if (projectile.explosionEffect != null) {
                Effecter effecter = projectile.explosionEffect.Spawn();
                effecter.Trigger(new TargetInfo(pawn.Position, pawn.Map), new TargetInfo(pawn.Position, pawn.Map));
                effecter.Cleanup();
            }

            IntVec3 position = pawn.Position;
            float explosionRadius = projectile.explosionRadius;
            DamageDef damageDef = projectile.damageDef;
            Thing launcher = pawn;
            int damageAmount = projectile.GetDamageAmount(1f);
            float armorPenetration = projectile.GetArmorPenetration(1f);
            SoundDef soundExplode = projectile.soundExplode;
            ThingDef postExplosionSpawnThingDef = projectile.postExplosionSpawnThingDef;
            ThingDef spawnThingDefWater = projectile.postExplosionSpawnThingDefWater;
            float postExplosionSpawnChance = projectile.postExplosionSpawnChance;
            int postExplosionSpawnThingCount = projectile.postExplosionSpawnThingCount;
            GasType? explosionGasType = projectile.postExplosionGasType;
            ThingDef preExplosionSpawnThingDef = projectile.preExplosionSpawnThingDef;
            float preExplosionSpawnChance = projectile.preExplosionSpawnChance;
            int preExplosionSpawnThingCount = projectile.preExplosionSpawnThingCount;
            float chanceToStartFire = projectile.explosionChanceToStartFire;;
            FloatRange? affectedAngle = new FloatRange?();
            float propagationSpeed = projectile.damageDef.expolosionPropagationSpeed;
            ThingDef postExplosionSpawnThingDefWater = spawnThingDefWater;
            float screenShakeFactor = projectile.screenShakeFactor;
            
            GenExplosion.DoExplosion(position, pawn.Map, explosionRadius, damageDef, launcher, damageAmount, armorPenetration,
                soundExplode, null, null, pawn, postExplosionSpawnThingDef, postExplosionSpawnChance, postExplosionSpawnThingCount,
                explosionGasType, projectile.applyDamageToExplosionCellsNeighbors, preExplosionSpawnThingDef, preExplosionSpawnChance, preExplosionSpawnThingCount, chanceToStartFire,
                projectile.explosionDamageFalloff, null, affectedAngle: affectedAngle, propagationSpeed: propagationSpeed,
                postExplosionSpawnThingDefWater: postExplosionSpawnThingDefWater, screenShakeFactor: screenShakeFactor);
        }
    }
}
