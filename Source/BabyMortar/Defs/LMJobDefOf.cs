﻿using RimWorld;
using Verse;

namespace BabyMortar {

    [DefOf]
    public static class LMJobDefOf {
        public static JobDef BeAShellJob;

        static LMJobDefOf() => DefOfHelper.EnsureInitializedInCtor(typeof(LMJobDefOf));
    }
}
