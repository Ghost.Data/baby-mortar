﻿using RimWorld;
using Verse;

namespace BabyMortar {

    [DefOf]
    public static class PawnProjectileDefOf {
        public static PawnProjectileDef Shell_Infant;

        static PawnProjectileDefOf() => DefOfHelper.EnsureInitializedInCtor(typeof(PawnProjectileDefOf));
    }
}
