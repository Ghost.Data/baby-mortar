﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace BabyMortar {
    public class PawnProjectileDef : ThingDef {
        public GeneDef gene;
        public ThoughtDef parentThought;
        public ThoughtDef colonistThought;
        public ThoughtDef thoughtInAreaOfImpact;
        public ThoughtDef parentThoughtForGunner;
        public ThoughtDef colonistThoughtForGunner;
        public ThoughtDef gunnerThought;
        public int psychologicalImpactRadius = 8;
    }
}
